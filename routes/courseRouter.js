const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  addSubject,
  getSubjects,
  getSubject,
  updateSubject,
  removeMaterialSubject,
  addMaterialSubject,
  enrollStudent
} = require('../controllers/courseController')
const {
  authorizeUserByRole,
} = require('../middlewares/authMiddleware');

const { sendResponse } = require('../utils/utils');
const { ROLES } = require('../utils/constants');
const { authorizeGetCourse, authorizeUpdateCourse } = require('../middlewares/authMiddleware');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
      req.body.imagePath = `public/`
      callback(null, req.body.imagePath);
  },
  filename: (req, file, callback) => {
      const splittedFileName = file.originalname?.split('.');
      const fileExtension = splittedFileName?.[splittedFileName?.length - 1];
      req.body.imageName = `${ Date.now() }_${ Math.random() }.${ fileExtension }`
      callback(null, req.body.imageName);
  },
});
// const storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     // Set the destination folder where uploaded files will be stored
//     cb(null, 'public/');
//   },
//   filename: (req, file, cb) => {
//     // Set the filename dynamically using the original filename
//     const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
//     cb(null, file.originalname + '-' + uniqueSuffix);
//   },
// });
  
const upload = multer({ storage: storage});

router.put(
  '/material/:id',
  upload.single('file'),
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER]),
  authorizeUpdateCourse,
  async (req, res, next) => {
  try {
    const result = await addMaterialSubject(req.params.id, { name: req.body.fileName, path: req.body.imageName, });
    return sendResponse(res, result);
  }
  catch(err) {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.delete(
  '/material/:id/:path',
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER]),
  authorizeUpdateCourse,
  async (req, res, next) => {
  try {
    const result = await removeMaterialSubject(req.params.id, req.params.path);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.get('/',
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER, ROLES.STUDENT, ROLES.PARENT]), 
  async (req, res, next) => {
  try {
    const result = await getSubjects(req.user);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.get(
  '/:id',
  // authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER, ROLES.STUDENT]),
  // authorizeGetCourse,
  async (req, res, next) => {
  try {
    const result = await getSubject(req.params.id);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const postSubjectSchema = Joi.object({
  name: Joi.string().required(),
  teacher: Joi.string().hex().length(24).required(),
  students: Joi.array().items(Joi.string().hex().length(24)),
});

router.post('/', 
  // validator.body(postSubjectSchema),
  // authorizeUserByRole([ROLES.ADMIN]),
  async (req, res, next) => {
  try {
    const result = await addSubject(req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const putSubjectSchema = Joi.object({
  name: Joi.string(),
  teacher: Joi.string().hex().length(24),
  students: Joi.array().items(Joi.string().hex().length(24)),
  assignments: Joi.array().items(Joi.string().hex().length(24)),
});

router.put(
  '/:id',
  validator.body(putSubjectSchema),
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER]),
  authorizeUpdateCourse,
  async (req, res, next) => {
  try {
    const result = await updateSubject(req.params.id, req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.patch('/enroll/:id', async (req, res) => {
  const { email } = req.body;
  try {
    const result = await enrollStudent(req.params.id, email);
    console.log(result)
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

module.exports = router;
