const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  addAnswer,
  getAnswer,
  updateAnswer,
} = require('../controllers/answerController')
const {
  authorizeUserByRole, authorizePostAnswer, authorizePutAnswer, authorizePutGradeAnswer,
} = require('../middlewares/authMiddleware');

const { sendResponse } = require('../utils/utils');
const { ROLES } = require('../utils/constants');
const { authorizeGetAnswer, } = require('../middlewares/authMiddleware');

router.get(
  '/:id',
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER, ROLES.STUDENT]),
  authorizeGetAnswer,
  async (req, res, next) => {
  try {
    const result = await getAnswer(req.params.id);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const postAnswerSchema = Joi.object({
  response: Joi.string().required(),
});

router.post(
  '/:testId',
  validator.body(postAnswerSchema),
  authorizeUserByRole([ROLES.STUDENT]),
  authorizePostAnswer,
  async (req, res, next) => {
  try {
    const result = await addAnswer(req.params.testId, req.body, req.user);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const putGradeAnswerSchema = Joi.object({
  grade: Joi.number().min(0).max(10).required(),
});

router.patch(
  '/grade/:id',
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER]),
  authorizePutGradeAnswer,
  async (req, res, next) => {
    console.log(req.body)
  try {
    const result = await updateAnswer(req.params.id, req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const putAnswerSchema = Joi.object({
  response: Joi.string().required(),
});

router.put(
  '/:id',
  validator.body(putAnswerSchema),
  authorizeUserByRole([ROLES.STUDENT]),
  authorizePutAnswer,
  async (req, res, next) => {
  try {
    const result = await updateAnswer(req.params.id, req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

module.exports = router;
