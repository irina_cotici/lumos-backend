const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  getUsers,
  addUser,
  toggleBlockUser,
  updateUser,
  getUser,
} = require('../controllers/userController')
const {
  authorizeUserByRole,
} = require('../middlewares/authMiddleware');

const {
  getGamificationPoints
} = require('../controllers/gamificationController')

const { sendResponse } = require('../utils/utils');
const { ROLES } = require('../utils/constants');
const User = require("../models/user");

const multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
      req.body.imagePath = `public/`
      callback(null, req.body.imagePath);
  },
  filename: (req, file, callback) => {
      const splittedFileName = file.originalname?.split('.');
      const fileExtension = splittedFileName?.[splittedFileName?.length - 1];
      req.body.imageName = `${ Date.now() }_${ Math.random() }.${ fileExtension }`
      callback(null, req.body.imageName);
  },
});

const upload = multer({ storage: storage});

router.get(
  '/users',
  async (req, res, next) => {
  try {
    const result = await getUsers(req.query.role, req.query.filter_by);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.put('/toggle_block/:id', authorizeUserByRole([ROLES.ADMIN]), async (req, res, next) => {
  try {
    const result = await toggleBlockUser(req.params.id);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});


router.patch('/update-user/:id',
  upload.single('file'),
  async (req, res) => {
    try {

      const user = await updateUser(req.params.id, req.body);

      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      res.json({ message: 'User updated successfully', user });
    } catch (err) {
      console.error('Failed to update user:', err);
      res.status(500).json({ message: 'Internal server error' });
    }
  }
);

router.patch('/update-score/:id',
  upload.single('file'),
  async (req, res) => {
    try {

      const user = await updateUser(req.params.id, req.body);

      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      res.json({ message: 'User updated successfully', user });
    } catch (err) {
      console.error('Failed to update user:', err);
      res.status(500).json({ message: 'Internal server error' });
    }
  }
);


router.get('/get-user/:id',
  async (req, res) => {
    try {

      const user = await getUser(req.params.id);

      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      res.json({ message: 'User returned', user });
    } catch (err) {
      console.error('Failed to update user:', err);
      res.status(500).json({ message: 'Internal server error' });
    }
  }
);

router.get('/gamification-points',
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER, ROLES.STUDENT, ROLES.PARENT]),
  async (req, res, next) => {
  try {
    const result = await getGamificationPoints(req.user)
    console.log(result)
    res.json({ message: 'Score returned', result });
  } catch (err) {
      console.error(err);
      return res.sendStatus(500);
  }
});

module.exports = router;
