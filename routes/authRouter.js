require('dotenv').config();
const express = require('express');
const axios = require('axios');
const JWT = require('jsonwebtoken');
var router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  JWT_KEY,
  JWT_SECRET,
  SESSION_TIME,
  ROLES,
} = require('../utils/constants');
const { sendResponse, } = require('../utils/utils');
const {
  checkUserCredentials,
  checkFacebookUserExists,
} = require('../controllers/authController');
const {
  addUser,
} = require('../controllers/userController');
const {
  authorizeUserByRole, auth
} = require('../middlewares/authMiddleware');
const queryString = require('node:querystring');
const crypto = require('crypto');
const User = require("../models/user");

const {
  getGamificationPoints
} = require('../controllers/gamificationController')

// Google OAuth2.0 config
const config = {
  clientId: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  authUrl: 'https://accounts.google.com/o/oauth2/v2/auth',
  tokenUrl: 'https://oauth2.googleapis.com/token',
  redirectUrl: process.env.REDIRECT_URL,
  clientUrl: process.env.CLIENT_URL,
  tokenSecret: process.env.TOKEN_SECRET,
  tokenExpiration: 36000,
};

const authParams = queryString.stringify({
  client_id: config.clientId,
  redirect_uri: config.redirectUrl,
  response_type: 'code',
  scope: 'openid profile email',
  access_type: 'offline',
  prompt: 'consent',
});

const getTokenParams = (code) => queryString.stringify({
  client_id: config.clientId,
  client_secret: config.clientSecret,
  code,
  grant_type: 'authorization_code',
  redirect_uri: config.redirectUrl,
});

router.get('/url', (_, res) => {
  // Random generated state for preventing csrf attacks
  authParams.state = crypto.randomBytes(20).toString('hex');
  res.json({
    url: `${config.authUrl}?${authParams}`,
  });
});

router.get('/token', async (req, res) => {
  const { state, code } = req.query;
  if (!code) return res.status(400).json({ message: 'Authorization code must be provided' });
  // Prevents csrf attacks
  if (state !== authParams.state) return res.status(400).json({ message: 'The session state is invalid' }); 
  try {
    // Get all parameters needed to hit authorization server
    const tokenParam = getTokenParams(code);
    // Exchange authorization code for access token (id token is returned here too)
    const { data: { id_token} } = await axios.post(`${config.tokenUrl}?${tokenParam}`);
    if (!id_token) return res.status(400).json({ message: 'Auth error' });
    // Get user info from id token
    const { email, name, picture } = JWT.decode(id_token);
    const user = { name, email, picture };
    // Sign a new token
    const token = JWT.sign({ user }, config.tokenSecret, { expiresIn: config.tokenExpiration });
    // Set cookies for user
    res.cookie('token', token, { maxAge: config.tokenExpiration, httpOnly: true,  })
    
    // find the user in the database
    const dataUser = await User.findOne({ email: user.email });
    if (!dataUser) {
      return res.json({ loggedIn: false });
    }

    let higherRole;

    if (dataUser.roles === ROLES.ADMIN) {
      higherRole = ROLES.ADMIN;
    }
    else if (dataUser.roles === ROLES.TEACHER) {
      higherRole = ROLES.TEACHER;
    }
    else if (dataUser.roles === ROLES.STUDENT) {
      higherRole = ROLES.STUDENT;
    }
    else if (dataUser.roles === ROLES.PARENT) {
      higherRole = ROLES.PARENT;
    }

    const authToken = JWT.sign({
      _id: dataUser._id,
      firstName: dataUser.firstName,
      lastName: dataUser.lastName,
    },
      JWT_KEY, {
      expiresIn: SESSION_TIME[higherRole]
    })

    res.json({
      user,
      token: authToken
    })
  } catch (err) {
    console.error('Error: ', err);
    res.status(500).json({ message: err.message || 'Server error' });
  }
});

router.get('/logged', async (req, res) => {
  try {
    // Get token from cookie
    const token = req.cookies.token;

    if (!token) return res.json({ loggedIn: false });
    const { user } = JWT.verify(token, config.tokenSecret);
    const newToken = JWT.sign({ user }, config.tokenSecret, { expiresIn: config.tokenExpiration });
    
    // find the user in the database
    const dataUser = await User.findOne({ email: user.email });
    if (!dataUser) {
      return res.json({ loggedIn: false });
    }
    
    const authToken = JWT.sign({ user }, config.tokenSecret, { expiresIn: config.tokenExpiration })

    res.cookie('token', newToken, { maxAge: config.tokenExpiration, httpOnly: true });
    res.json({ loggedIn: true, user, token: authToken });

  } catch (err) {
    res.json({ loggedIn: false });
  }
});

router.get('/loggedUser', async (req, res) => {
  try {
    const token = req.headers['authorization']

    if (!token) {
      return res.status(401).json({ message: 'No token provided' })
    }
    
    const decoded = JWT.verify(token, JWT_KEY)
    const user = await User.findById(decoded._id)

    if (!user) {
      return res.status(404).json({ message: 'User not found' })
    }

    const userData = {
      _id: user._id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      role: user.role,
      birthday: user.birthday,
      badges: user.badges,
      phone: user.phone,
      description: user.description,
      image: user.image,
      children: user.children
    }

    res.json(userData)

  } catch (error) {
    console.log(error)
    res.status(500).json({ message: 'Internal server error' })
  }
})

router.post('/logout', (_, res) => {
  // clear cookie
  res.clearCookie('token').json({ message: 'Logged out' });
});

const postUserSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(8).required(),
  confirmPassword: Joi.string().min(8).required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  role: Joi.string().required(),
  phone: Joi.string(),
  birthday: Joi.string(),
});

router.post(
  '/add-user',
  validator.body(postUserSchema),
  async (req, res, next) => {
  try {
    const result = await addUser(req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const postLoginSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(8).required(),
});

router.post('/login', validator.body(postLoginSchema), async (req, res) => {
  console.log()
  try {
    var user = await checkUserCredentials(req.body.email, req.body.password);

    if (user == null) {
      res.statusCode = 401;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false, status: 'Wrong email or password!' });
      return;
    }
    else if (user.blocked) {
      res.statusCode = 401;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false, status: 'This user is blocked!' });
      return;
    }
    else {
      let higherRole;

      if (user.role === ROLES.ADMIN ) {
        higherRole = ROLES.ADMIN;
      }
      else if (user.role === ROLES.TEACHER) {
        higherRole = ROLES.TEACHER;
      }
      else if (user.role === ROLES.STUDENT) {
        higherRole = ROLES.STUDENT;
      }
      else if (user.role === ROLES.PARENT) {
        higherRole = ROLES.PARENT;
      }

      const token = JWT.sign({
        _id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
      },
        JWT_KEY, {
        expiresIn: SESSION_TIME[higherRole]
      })

      res.status(200).json({
        _id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        roles: user.roles,
        blocked: user.blocked,
        image: user.image,
        role: user.role,
        token,
      });
    }
  }
  catch (err) {
    console.log(err)
    return res.statusCode(500);
  }
})


router.get('/user_data', authorizeUserByRole([0]), async (req, res, next) => {
  try {
    return sendResponse(res, {
      status: true,
      payload: req.userData,
    });
  }
  catch (err) {
    console.log(err);
    return res.sendStatus(500);
  }
});

module.exports = router;