const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  addAssignment,
  getAssignment,
  updateAssignment,
  removeAssignment,
  getTest
} = require('../controllers/testController')
const {
  authorizeUserByRole, authorizePostTest, authorizePutTest,
} = require('../middlewares/authMiddleware');

const { sendResponse } = require('../utils/utils');
const { ROLES } = require('../utils/constants');
const { authorizeGetTest, } = require('../middlewares/authMiddleware');

router.get(
  '/:id',
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER, ROLES.STUDENT, ROLES.PARENT]),
  // authorizeGetTest,
  async (req, res, next) => {
  try {
    const result = await getTest(req.params.id, req.user);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const postAssignmentSchema = Joi.object({
  type: Joi.string().required(),
  question: Joi.string().required(),
  variants: Joi.string().required(),
});

router.post(
  '/:subjectId',
  validator.body(postAssignmentSchema),
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER]),
  authorizePostTest,
  async (req, res, next) => {
  try {
    const result = await addAssignment(req.params.subjectId, req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const putSubjectSchema = Joi.object({
  type: Joi.string(),
  question: Joi.string(),
  variants: Joi.string(),
});

router.put(
  '/:id',
  validator.body(putSubjectSchema),
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER]),
  authorizePutTest,
  async (req, res, next) => {
  try {
    const result = await updateAssignment(req.params.id, req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.delete(
  '/:id',
  authorizeUserByRole([ROLES.ADMIN, ROLES.TEACHER]),
  authorizePutTest,
  async (req, res, next) => {
  try {
    const result = await removeAssignment(req.params.id);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

module.exports = router;
