describe('User Routes', () => {
  it('registers a new user', () => {  
    cy.fixture('register').then((testUser) => {  
      cy.request({
        method: 'POST',
        url: 'http://localhost:3001/auth/add-user', 
        body: testUser, 
      }).then((response) => {  
        expect(response.status).to.eq(200); 
      });
    });
  });

  it('login a user', () => {  
    cy.fixture('login').then((testUser) => {  
      cy.request({
        method: 'POST',
        url: 'http://localhost:3001/auth/login', 
        body: testUser, 
      }).then((response) => {  
        expect(response.status).to.eq(200); 
      });
    });
  });

  it('login with wrong credentials', () => {  
    cy.fixture('wrong_login').then((testUser) => {  
      cy.request({
        method: 'POST',
        url: 'http://localhost:3001/auth/login', 
        body: testUser, 
        failOnStatusCode: false
      }).then((response) => {  
        expect(response.status).to.eq(401); 
        expect(response.body.status).to.eq('Wrong email or password!');
      });
    });
  });

  it('update a user', () => {  
    cy.fixture('update').then((data) => {  
      cy.request({
        method: 'PATCH',
        url: `http://localhost:3001/user/update-user/${data.user_id}`, 
        body: data.image, 
      }).then((response) => {  
        expect(response.status).to.eq(200); 
      });
    });
  });

  it('get a user', () => {  
    cy.fixture('update').then((data) => {  
      cy.request({
        method: 'GET',
        url: `http://localhost:3001/user/get-user/${data.user_id}`, 
        body: data.image, 
      }).then((response) => {  
        expect(response.status).to.eq(200); 
      });
    });
  });

  it('get users', () => {  
    cy.fixture('update').then((data) => {  
      cy.request({
        method: 'GET',
        url: `http://localhost:3001/user/users`, 
        body: data.image, 
      }).then((response) => {  
        expect(response.status).to.eq(200); 
      });
    });
  });
})