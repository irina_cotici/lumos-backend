describe('User Routes', () => {
  // it('registers a new user', () => {  
  //   cy.fixture('register').then((testUser) => {  
  //     cy.request({
  //       method: 'POST',
  //       url: 'http://localhost:3001/auth/add-user', 
  //       body: testUser, 
  //     }).then((response) => {  
  //       expect(response.status).to.eq(200); 
  //     });
  //   });
  // });

  it('login a user', () => {  
    cy.fixture('login').then((testUser) => {  
      cy.request({
        method: 'POST',
        url: 'http://localhost:3001/auth/login', 
        body: testUser, 
      }).then((response) => {  
        expect(response.status).to.eq(200); 
      });
    });
  });

  it('update a user', () => {  
    cy.fixture('update').then((data) => {  
      cy.request({
        method: 'PATCH',
        url: `http://localhost:3001/user/update-user/${data.user_id}`, 
        body: data.image, 
      }).then((response) => {  
        expect(response.status).to.eq(200); 
      });
    });
  });

  it('update a user', () => {  
    cy.fixture('update').then((data) => {  
      cy.request({
        method: 'PATCH',
        url: `http://localhost:3001/user/update-user/${data.user_id}`, 
        body: data.image, 
      }).then((response) => {  
        expect(response.status).to.eq(200); 
      });
    });
  });
})