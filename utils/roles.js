const roles = {
  admin: {
    id: "admin",
    permissions: [
      "createUser", "deleteUser", "editUser", 
      "createCourse", "deleteCourse", "editCourse", "viewCourse", 
      "enrollCourse", "manageMaterials", "awardPoints", "viewReports"
    ],
  },
  teacher: {
    id: "teacher",
    permissions: [
      "createCourse", "deleteCourse", "editCourse", "viewCourse", 
      "enrollCourse", "manageMaterials", "awardPoints", "viewReports"
    ],
  },
  student: {
    id: "student",
    permissions: [
      "viewCourse", "enrollCourse", "viewMaterials", "submitAssignments", "viewOwnPoints"
    ],
  },
  parent: {
    id: "parent",
    permissions: [
      "viewCourse", "viewChildProgress", "viewChildPoints"
    ],
  },
};

const permissions = {
  createUser: {
    id: "createUser",
    description: "Permite crearea de noi conturi de utilizatori."
  },
  deleteUser: {
    id: "deleteUser",
    description: "Permite ștergerea conturilor de utilizatori."
  },
  editUser: {
    id: "editUser",
    description: "Permite editarea conturilor de utilizatori."
  },
  createCourse: {
    id: "createCourse",
    description: "Permite utilizatorului să creeze cursuri."
  },
  deleteCourse: {
    id: "deleteCourse",
    description: "Permite utilizatorului să șteargă cursuri."
  },
  editCourse: {
    id: "editCourse",
    description: "Permite utilizatorului să editeze cursuri."
  },
  viewCourse: {
    id: "viewCourse",
    description: "Permite utilizatorului să vizualizeze cursuri."
  },
  enrollCourse: {
    id: "enrollCourse",
    description: "Permite înscrierea la cursuri."
  },
  manageMaterials: {
    id: "manageMaterials",
    description: "Permite gestionarea materialelor didactice."
  },
  awardPoints: {
    id: "awardPoints",
    description: "Permite oferirea de puncte și insigne elevilor."
  },
  viewReports: {
    id: "viewReports",
    description: "Permite vizualizarea rapoartelor de activitate și progres."
  },
  viewMaterials: {
    id: "viewMaterials",
    description: "Permite elevilor să acceseze materialele de curs."
  },
  submitAssignments: {
    id: "submitAssignments",
    description: "Permite elevilor să trimită sarcini și teste."
  },
  viewOwnPoints: {
    id: "viewOwnPoints",
    description: "Permite elevilor să-și vizualizeze punctele și insignele acumulate."
  },
  viewChildProgress: {
    id: "viewChildProgress",
    description: "Permite părinților să vizualizeze progresul academic al copiilor."
  },
  viewChildPoints: {
    id: "viewChildPoints",
    description: "Permite părinților să vizualizeze punctele și insignele copiilor."
  },
};

const userRoles = [
  { userId: "user1", roleId: "admin" },
  { userId: "user2", roleId: "teacher" },
  { userId: "user3", roleId: "student" },
  { userId: "user4", roleId: "parent" },
];
