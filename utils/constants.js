module.exports = {
    JWT_KEY: "ASFASF#$ASDSDADASDCASVMICECENCECUENDAS",
    MONGO_URL: 'mongodb://localhost:27017/ByteLearn',
    ROLES: {
        STUDENT: 'STUDENT',
        TEACHER: 'TEACHER',
        ADMIN: 'ADMIN',
        PARENT: 'PARENT',
    },
    SESSION_TIME: {
        STUDENT: '8h',
        TEACHER: '4h',
        ADMIN: '1h',
        PARENT: '1h',
    },
    JWT_SECRET: "eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTcxNzUzMTkwNCwiaWF0IjoxNzE3NTMxOTA0fQ.jMqQj1MzA_czmTvMLs9Ti0rgqO"
}
