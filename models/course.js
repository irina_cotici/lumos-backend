const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const materialSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    path: {
        type: String,
        required: true,
    },
});

const subjectSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    image: {
        type: String,
    },
    schedule: {
        type: [String],
    },
    teacher: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        // required: true,
    },
    students: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'User',
        default: [],
    },
    tests: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Test',
        default: [],
    },
    modules: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Module',
        default: [],
    }
});

const Subject = mongoose.model('Subject', subjectSchema);

module.exports = Subject;
