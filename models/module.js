const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const moduleSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        default: '',
    },
    image: {
        type: String,
        default: ''
    },
    duration: {
        type: String,
        default: ''
    },
    resources: {
        type: [String],
        ref: 'Resource',
        default: []
    },
});

const Module = mongoose.model('Module', moduleSchema);

module.exports = Module;
