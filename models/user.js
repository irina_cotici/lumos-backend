const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        default: '',
    }, 
    role: {
        type: String,
        required: true,
    },
    badges: {
        type: [String],
        required: false,
    },
    children: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'User'
    },
    blocked: {
        type: Boolean,
        default: false,
    },
    image: {
        type: String,
        default: '',
    },
    description: {
        type: String,
        default: '',
    },
    birthday: {
        type: String,
        default: '',
    },
    phone: {
        type: String,
        default: '',
    },
    score: {
        type: Number,
        default: 0
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
