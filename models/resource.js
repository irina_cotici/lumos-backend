const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const resourceSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        default: '',
    },
    type: {
        type: String,
        default: ''
    },
});

const Resource = mongoose.model('Resource', resourceSchema);

module.exports = Resource;
