const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const answerSchema = new Schema({
    student: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    response: {
        type: String,
        required: true,
    },
    grade: {
        type: Number,
    },
});

const Answer = mongoose.model('Answer', answerSchema);

module.exports = Answer;
