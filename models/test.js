const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const testSchema = new Schema({
    type: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        default: '',
    },
    maximumScore: {
        type: Number,
        default: 10,
    },
    image: {
        type: String,
        default: ''
    },
    duration: {
        type: String,
        default: ''
    },
    deadline: {
        type: String,
        default: ''
    },
    questions: {
        type: [String],
        default: [],
    },
    answers: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Answer',
        default: [],
    },
});

const Test = mongoose.model('Test', testSchema);

module.exports = Test;
