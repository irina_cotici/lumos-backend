const User = require("../models/user");
const Course = require("../models/course");
const JWT = require("jsonwebtoken");
const { ROLES } = require("../utils/constants");
const {
    getSubjects
  } = require('../controllers/courseController')

  const getCoursesByUserRole = async (user) => {
    try {
      let courses;
      if (user.role === ROLES.ADMIN) {
        courses = await Course.find({});
      } else if (user.role === ROLES.TEACHER) {
        courses = await Course.find({ teacher: user._id });
      } else if (user.role === ROLES.STUDENT) {
        courses = await Course.find({ students: user._id });
      } else if (user.role === ROLES.PARENT) {
        const children = await User.find({ parent: user._id });
        const childrenIds = children.map(child => child._id);
        courses = await Course.find({ students: { $in: childrenIds } });
      } else {
        return [];
      }
      return courses;
    } catch (err) {
      console.error(err);
      throw new Error('Error fetching courses');
    }
  };

const getGamificationPoints = async (user) => {
    try {
        const courses = await getCoursesByUserRole(user);
        const courseTitles = courses.map(course => course.title);
        
        const userPoints = []
        const coursesData = await Promise.all(courses.map(async (course) => {
            const students = await User.find({ _id: { $in: course.students } }, 'score'); // Only fetching score
            const totalPoints = students.reduce((sum, student) => sum + student.score, 0) - user.score;
            userPoints.push(Math.ceil(user.score / courses.length) || 0)
            let studentsScores = [];
            if (user.role === ROLES.STUDENT) {
              const currentUserScore = students.find(student => student._id.equals(user._id));
              studentsScores = currentUserScore ? [currentUserScore.score] : [];
            }
      
            return {
              studentsScores,
              totalPoints
            };
          }));

          const totalPointsByCourse = coursesData.map(courseData => courseData.totalPoints);
        return {
            status: true,
            payload: {
                totalPointsByCourse,
                courseTitles,
                userPoints
            },
        };

    } catch (err) {
        console.log(err)
        return false;
    }
}


module.exports = {
    getGamificationPoints
}
