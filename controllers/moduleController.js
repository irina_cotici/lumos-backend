const Course = require("../models/course");
const Module = require("../models/module");
const Test = require("../models/test");
const User = require("../models/user");
const { ROLES } = require("../utils/constants");

const getModule = async (moduleId) => {
    try {
        const module = await Module.findOne({ _id: moduleId, })
            .populate({  path : 'resources', model: 'Resource', })

        return {
            status: true,
            payload: module,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const updateModule = async (id, subjectObj) => {
    try {
        const subject = await Course.updateOne({ _id: id }, subjectObj);
        
        return {
            status: true,
            payload: subject,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

module.exports = {
    getModule
}
