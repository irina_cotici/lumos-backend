const User = require("../models/user");
const JWT = require("jsonwebtoken");
const { ROLES } = require("../utils/constants");

const addUser = async (userObj) => {
    try {
        const checkUsers = await User.find({ $or: [
            { email: userObj.email },
        ] });

        if (checkUsers?.length !== 0) {
            return {
                status: false,
                payload: "Email should be unique",
            }
        }

        const user = new User(userObj);
        await user.save();
        
        return {
            status: true,
            payload: user,
        };
    } catch (err) {
        console.log(err);
        return false;
    }
}

const getUsers = async (role, filter_by) => {
    try {
        let users = await User.find().sort({ score: -1 })
        
        if (role === ROLES.TEACHER) {
            users = users.filter(user => user.role !== ROLES.ADMIN);
        } else if (role === ROLES.STUDENT) {
            users = users.filter(user => user.role !== ROLES.ADMIN && user.role !== ROLES.TEACHER && user.role !== ROLES.PARENT);
        }

        if (filter_by === ROLES.TEACHER) {
            users = users.filter(user => user.role === ROLES.TEACHER);
            console.log(users)
        }
        return {
            status: true,
            payload: users,
        };
    } catch (err) {
        return false;
    }
}

const toggleBlockUser = async (id) => {
    try {
        const user = await User.findOne({ _id: id });

        if (!user) {
            return {
                status: false,
                payload: "User not found",
            };
        }
        else if (user.roles.includes(ROLES.ADMIN)) {
            return {
                status: false,
                payload: "This user is an admin",
            };
        }

        user.blocked = !user.blocked;
        await user.save();
        
        return {
            status: true,
            payload: user,
        };
    } catch (err) {
        return false;
    }
}

const updateUser = async (id, userDetails) => {
    try {
        console.log(userDetails.children, Array.isArray(userDetails.children))
        if (userDetails.children && Array.isArray(userDetails.children) && typeof userDetails.children[0] !== 'object') {
            // Find users by emails
            const children = await User.find({ email: { $in: userDetails.children } }).exec();
            // Get the IDs of the children
            userDetails.children = children.map(child => child._id);
        }
        const user = await User.findOneAndUpdate({ _id: id }, userDetails, { new: true });
        return {
            status: true,
            payload: user,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getUser = async (id) => {
    try {
        const user = await User.findOne({ _id: id }).populate({  path : 'children', select: 'firstName lastName email image', model: 'User', });
        console.log(user)
        return {
            status: true,
            payload: user,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

module.exports = {
    getUsers,
    addUser,
    toggleBlockUser,
    updateUser,
    getUser
}
