const Answer = require("../models/answer");
const Test = require("../models/test");
const Course = require("../models/course");
const { ROLES } = require("../utils/constants");

const addAnswer = async (assignmentId, answerObj, user) => {
    try {
        const assignment = await Test.findById(assignmentId)
            .populate({  path : 'answers', model: 'Answer', });
        
        if (assignment.answers.find(answer => answer.student.equals(user._id))) {
            return {
                status: false,
                payload: "You already answered to this assignment",
            };
        }
        
        const answer = new Answer({...answerObj, student: user._id});
        await answer.save();
        assignment.answers.push(answer._id);
        await assignment.save();
        
        return {
            status: true,
            payload: answer,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getAnswer = async (id) => {
    try {
        let assignment;
        
        assignment = await Answer.findOne({ _id: id, })
            .populate({  path : 'student', model: 'User', });
        return {
            status: true,
            payload: assignment,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const updateAnswer = async (id, assignmentObj) => {
    try {
        const assignment = await Answer.updateOne({ _id: id }, assignmentObj);
        const test = await Test.findOne({ answers: id })
        .populate({  path : 'answers', model: 'Answer', populate : {
            path : 'student', model: 'User'
        } })
        return {
            status: true,
            payload: test,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

module.exports = {
    addAnswer,
    getAnswer,
    updateAnswer,
}
