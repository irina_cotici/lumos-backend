const Test = require("../models/test");
const Course = require("../models/course");
const { ROLES } = require("../utils/constants");

const addAssignment = async (subjectId, assignmentObj) => {
    try {
        const subject = await Course.findById(subjectId);
        const assignment = new Test(assignmentObj);
        await assignment.save();
        subject.assignments.push(assignment._id);
        await subject.save();
        
        return {
            status: true,
            payload: assignment,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getAssignment = async (id, user) => {
    try {
        let assignment = await Test.findOne({ _id: id, })
            .populate({  path : 'answers', model: 'Answer', populate : {
                path : 'student', model: 'User'
            } })
        
        if (user.roles?.length === 0 && user.role === ROLES.STUDENT) {
            assignment.answers = assignment.answers.filter(answer => answer.student._id === user._id);
        }

        return {
            status: true,
            payload: assignment,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getTest = async (id, user) => {
    try {
        let test = await Test.findOne({ _id: id, })
        .populate({  path : 'answers', model: 'Answer', populate : {
            path : 'student', model: 'User'
        } });

        
        if (user.role === ROLES.STUDENT) {
            test.answers = test.answers.filter(answer => { 
                return answer.student._id.toString() === user._id.toString() });
        }
        
        return {
            status: true,
            payload: test,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const updateAssignment = async (id, assignmentObj) => {
    try {
        const assignment = await Test.updateOne({ _id: id }, assignmentObj);
        
        return {
            status: true,
            payload: assignment,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const removeAssignment = async (assignmentId) => {
    try {
        const assignment = await Test.findById(assignmentId);
        if (!assignment) {
            return { status: false, message: "Assignment not found." };
        }

        const subject = await Course.findOne({ assignments: assignmentId });
        if (!subject) {
            return { status: false, message: "Subject not found for this assignment." };
        }

        subject.assignments.pull(assignmentId);
        await subject.save();

        await Assignment.deleteOne({ _id: assignmentId });

        return {
            status: true,
            message: "Assignment removed successfully."
        };
    } catch (err) {
        console.log(err)
        return { status: false, message: "Error occurred while removing the assignment." };
    }
}

module.exports = {
    addAssignment,
    getAssignment,
    updateAssignment,
    removeAssignment,
    getTest
}
