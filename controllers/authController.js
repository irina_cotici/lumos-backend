const User = require("../models/user");

const checkUserCredentials = async (email, password) => {
    try {
        var user = await User.findOne({ email: email, password: password })
        return user;
    }
    catch(err) {
        return false;
    }
}

const checkFacebookUserExists = async (email) => {
    try {
        var user = await User.findOne({ email: email })
        return user;
    }
    catch(err) {
        return false;
    }
}

module.exports = {
    checkUserCredentials,
    checkFacebookUserExists,
}
