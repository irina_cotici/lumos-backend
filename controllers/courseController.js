const Course = require("../models/course");
const Module = require("../models/module");
const Test = require("../models/test");
const User = require("../models/user");
const Resource = require("../models/resource");
const { ROLES } = require("../utils/constants");

const addSubject = async (subjectObj) => {
    const { modules: moduleObjs, tests: testObjs, teacher: teacherId, ...subjectData } = subjectObj;

    try {
        // Save modules with resources
        const modules = await Promise.all(moduleObjs.map(async mod => {
            const { resources: resourceObjs, ...moduleData } = mod;
            const moduleResources = resourceObjs.map(resource => new Resource(resource));
            const newModule = new Module({
                ...moduleData,
                resources: moduleResources.map(resource => resource._id)
            });
            await Promise.all(moduleResources.map(resource => resource.save()));
            return newModule.save();
        }));

        // Save tests with questions
        const tests = await Promise.all(testObjs.map(async test => {
            console.log(test)
            const newTest = new Test(test);
            return newTest.save();
        }));

        // Create and save the course
        const course = new Course({
            ...subjectData,
            modules: modules.map(mod => mod._id),
            tests: tests.map(test => test._id),
            teacher: teacherId
        });

        const teacher = await User.findById(teacherId);

        const coursePayload = {
            ...course.toObject(),
            teacher: {
                firstName: teacher.firstName,
                lastName: teacher.lastName,
                imageUrl: teacher.image
            }
        };

        await course.save();
        
        return {
            status: true,
            payload: coursePayload,
        };

    } catch (err) {
        console.log(err);
        return {
            status: false,
            error: err.message
        };
    }
}

const getSubjects = async (user) => {
    console.log('aicea')
    try {
        let query;
        if (user.role === ROLES.ADMIN) {
            query = {};
        }
        else if (user.role === ROLES.TEACHER) {
            query = { teacher: user._id }
        }
        else if (user.role === ROLES.STUDENT) {
            query = { students: { $in: user._id } }
        } else {
            const children = await User.find({ parent: user._id });
            const childrenIds = children.map(child => child._id);
            query = { students: { $in: childrenIds } }
        }
        
        const subject = await Course.find(query)
            .populate({  path : 'teacher', select:'-password', model: 'User', })

        return {
            status: true,
            payload: subject,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getSubject = async (subjectId) => {
    try {
        const subject = await Course.findOne({ _id: subjectId, })
            .populate({  path : 'teacher', select:'-password', model: 'User', })
            .populate({  path : 'students', select:'-password', model: 'User', })
            .populate({  path : 'modules', select:'title description duration', model: 'Module', })
            .populate({  path : 'tests', select:'title description duration type', model: 'Test', })
        
        return {
            status: true,
            payload: subject,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const updateSubject = async (id, subjectObj) => {
    try {
        const subject = await Course.updateOne({ _id: id }, subjectObj);
        
        return {
            status: true,
            payload: subject,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const addMaterialSubject = async (id, imageObj) => {
    try {
        const subject = await Course.findById(id);
        subject.materials.push(imageObj);
        await subject.save();
        
        return {
            status: true,
            payload: subject,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const removeMaterialSubject = async (id, path) => {
    try {
        const subject = await Course.findById(id);
        subject.materials = subject.materials.filter(material => material.path !== path);
        await subject.save();
        
        return {
            status: true,
            payload: subject,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const enrollStudent = async (courseId, studentEmail) => {
    try {
        const student = await User.findOne({ email: studentEmail });
        
        if (!student) {
            return { status: false, error: "Student not found." };
        }

        const course = await Course.findById(courseId);
        if (!course) {
            return { status: false, error: "Course not found." };
        }

        if (course.students.includes(student._id)) {
            return { status: false, error: "Student already enrolled in this course." };
        }

        course.students.push(student._id);
        await course.save();

        await Course.populate(course, [
            { path: 'teacher', select: '-password', model: 'User' },
            { path: 'students', select: '-password', model: 'User' },
            { path: 'modules', select: 'title description duration', model: 'Module' },
            { path: 'tests', select: 'title description duration type', model: 'Test' }
        ]);

        return {
            status: true,
            payload: course
        };
    } catch (err) {
        console.error("Failed to enroll student:", err);
        return { status: false, error: err.message };
    }
}


module.exports = {
    addSubject,
    getSubjects,
    getSubject,
    updateSubject,
    addMaterialSubject,
    removeMaterialSubject,
    enrollStudent
}
