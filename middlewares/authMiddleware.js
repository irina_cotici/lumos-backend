const User = require("../models/user");
const JWT = require("jsonwebtoken");
const { JWT_KEY, ROLES, } = require("../utils/constants");
const Course = require("../models/course");
const Test = require("../models/test");
const Answer = require("../models/answer");

const authorizeUserByRole = (roles) => async(req, res, next) => {
    if (roles === null || roles === undefined) {
        res.status(500).json({ message: "Internal server error" })
        next();
    }
    else if (req.headers.authorization) {
        const token = req.headers['authorization'];
        
        JWT.verify(token, JWT_KEY, async(err, payload) => {
            if (err) {
                console.log('err', err)
                res.status(403).json({ message: "Invalid token, please login again" })
            } 
            else {
                try {
                    const user = await User.findOne({ _id: payload._id })
                    const hasAccess = roles.some(role => user.role === role);
                    
                    req.user = user;
                    
                    if (user.blocked) {
                        res.status(403).json({ message: "Your account is blocked"})
                    } 
                    else if (!hasAccess) {
                        res.status(403).json({ message: "Your don't have rights for this operation"})
                    } 
                    else {
                        next();
                    }
                } 
                catch (error) {
                    res.sendStatus(500);
                }
            }
        })
    } 
    else {
        res.status(401).json({ message: "Please provide identity token" })
    }
}

const authorizeGetCourse = async(req, res, next) => {
    const course = await Course.findById(req.params.id)

    if (!course) {
        res.status(500).json({ message: "Wrong Course id" })
    }
    
    if (
        req.user.roles === ROLES.ADMIN ||
        (req.user.roles === ROLES.TEACHER && req.user._id.equals(course.teacher)) ||
        (req.user.roles === ROLES.STUDENT && course.students.find(student => student.equals(req.user._id)))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to access this Course" })
    }
}

const authorizeUpdateCourse = async(req, res, next) => {
    const course = await Course.findById(req.params.id)

    if (!course) {
        res.status(500).json({ message: "Wrong Course id" })
    }
    
    if (
        req.user.roles.includes(ROLES.ADMIN) ||
        (req.user.roles.includes(ROLES.TEACHER) && req.user._id.equals(course.teacher))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to update this Course" })
    }
}

const authorizeGetTest = async(req, res, next) => {
    const course = await Course.findOne({ tasks: req.params.id})

    if (!course) {
        res.status(500).json({ message: "Wrong Testid" })
    }
    
    if (
        req.user.roles.includes(ROLES.ADMIN) ||
        (req.user.roles.includes(ROLES.TEACHER) && req.user._id.equals(course.teacher)) ||
        (req.user.roles.includes(ROLES.STUDENT) && course.students.find(student => student.equals(req.user._id)))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to access this assignment" })
    }
}

const authorizePostTest = async(req, res, next) => {
    const course = await Course.findById(req.params.courseId)

    if (!course) {
        res.status(500).json({ message: "Wrong Test id" })
    }
    
    if (
        req.user.roles.includes(ROLES.ADMIN) ||
        (req.user.roles.includes(ROLES.TEACHER) && req.user._id.equals(course.teacher))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to add a assignment" })
    }
}

const authorizePutTest = async(req, res, next) => {
    const course = await Course.find({ tasks: req.params.id})

    if (!course) {
        res.status(500).json({ message: "Wrong assignment id" })
    }
    
    if (
        req.user.roles.includes(ROLES.ADMIN) ||
        (req.user.roles.includes(ROLES.TEACHER) && req.user._id.equals(course.teacher))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to add a assignment" })
    }
}

const authorizeGetAnswer = async(req, res, next) => {
    const answer = await Answer.findById(req.params.id)
    const test = await Test.findOne({ answers: req.params.id})
    const course = await Course.findOne({ tasks: test?._id})

    if (!course) {
        res.status(500).json({ message: "Wrong answer id" })
    }
    
    if (
        req.user.roles.includes(ROLES.ADMIN) ||
        (req.user.roles.includes(ROLES.TEACHER) && req.user._id.equals(course.teacher)) ||
        (req.user.roles.includes(ROLES.STUDENT) && req.user._id.equals(answer.student))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to access this answer" })
    }
}

const authorizePostAnswer = async(req, res, next) => {
    const course = await Course.findOne({ tests: req.params.testId})

    if (!course) {
        res.status(500).json({ message: "Wrong assignment id" })
    }
    
    if (
        req.user.role === ROLES.ADMIN ||
        (req.user.role === ROLES.STUDENT && course.students.find(student => student.equals(req.user._id)))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to access this assignment" })
    }
}

const authorizePutAnswer = async(req, res, next) => {
    const answer = await Answer.findById(req.params.id)
    const test = await Test.findOne({ answers: req.params.id})
    const course = await Course.findOne({ tasks: test?._id})

    if (!course) {
        res.status(500).json({ message: "Wrong answer id" })
    }
    
    if (
        req.user.role === ROLES.ADMIN ||
        (req.user.role === ROLES.STUDENT && req.user._id.equals(answer.student))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to edit this answer" })
    }
}

const authorizePutGradeAnswer = async(req, res, next) => {
    const test = await Test.findOne({ answers: req.params.id})
    const course = await Course.findOne({ tests: test?._id})

    if (!course) {
        res.status(500).json({ message: "Wrong answer id" })
    }
    
    if (
        req.user.role === ROLES.ADMIN ||
        (req.user.role === ROLES.TEACHER && req.user._id.equals(course.teacher))
    ) {
        next();
    }
    else {
        res.status(401).json({ message: "You do not have rights to grade this answer" })
    }
}

module.exports = {
    authorizeUserByRole,
    authorizeGetCourse,
    authorizeUpdateCourse,
    authorizeGetTest,
    authorizePostTest,
    authorizePutTest,
    authorizeGetAnswer,
    authorizePostAnswer,
    authorizePutAnswer,
    authorizePutGradeAnswer,
}