#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('../app');
var debug = require('debug')('ua-back:server');
var http = require('http');
const { Server } = require('ws');

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3001');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Connect ws
 */

const wss = new Server({ server });

const clients = new Map();

wss.on('connection', (ws) => {
  console.log('Client connected');

  ws.on('message', (message) => {
      const data = JSON.parse(message);
      if (data.type === 'register') {
          // Register user ID with this WebSocket
          clients.set(data._id, ws);
          console.log(`Registered user ID ${data._id}`);
      } else {
          // Handle other message types or routing
          if (data.destination && clients.has(data.destination)) {
              const targetWs = clients.get(data.destination);
              targetWs.send(JSON.stringify(data));
          }
      }
  });

  ws.on('close', () => {
      // Remove the WebSocket from the map when it is closed
      clients.forEach((value, key) => {
          if (value === ws) {
              clients.delete(key);
              console.log(`Connection with user ID ${key} closed`);
          }
      });
  });
});

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    console.log("Backend runs on port:", port)
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
